<?php

    use Illuminate\Support\Facades\Route;
    use Illuminate\Support\Facades\Auth;

    /*
    |--------------------------------------------------------------------------
    | Web Routes
    |--------------------------------------------------------------------------
    |
    | Here is where you can register web routes for your application. These
    | routes are loaded by the RouteServiceProvider within a group which
    | contains the "web" middleware group. Now create something great!
    |
    */

    Route::get('/', function () {
        if (Auth::check()) {
            return redirect(\route('home'));
        } else {
            return redirect(\route('login'));
        }
    });

    Auth::routes();

    Route::get('/home', 'HomeController@index')->name('home');

    Route::group(['middleware' => 'auth'], function () {
        Route::get('/tags', 'TagsController@index')->name('tags.index');
        Route::post('/tags/create', 'TagsController@store')->name('tags.store');
        Route::delete('/tags/delete/{tags}', 'TagsController@destroy')->name('tags.destroy');
    });

    Route::resource('categories', 'CategoriesController')->parameters([
        'categories' => 'categories'
    ])->except(['create'])->middleware('auth');
    Route::get('/categories/edit/{categories}', ['as' => 'categories.edit', 'uses' => 'CategoriesController@edit'])->middleware('auth');


    Route::resource('post', 'PostController')->middleware('auth');

    Route::get('gallery/{id}', 'GalleryController@index')->name('gallery.index');
    Route::get('display/{id}', 'GalleryController@show')->name('gallery.show');
    Route::get('download/{id}', 'GalleryController@download')->name('gallery.download');
    Route::post('refresh/{id}', 'GalleryController@refresh')->name('gallery.refresh');
    Route::post('upload/{id}', 'GalleryController@upload')->name('gallery.upload');
    Route::resource('gallery', 'GalleryController')->middleware('auth')->except([
        'index', 'update', 'store', 'show', 'create', 'upload', 'download', 'refresh'
    ]);
