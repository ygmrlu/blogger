<?php

use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;

Breadcrumbs::for('home', function ($trail) {
    $trail->push('Anasayfa', url('/'));
});

// Categories Breadcrumbs
Breadcrumbs::for('categories', function ($trail) {
    $trail->parent('home');
    $trail->push('Kategoriler', route('categories.index'));
});

Breadcrumbs::for('show', function ($trail, $categoryName) {
    $trail->parent('categories');
    $trail->push($categoryName);
});

Breadcrumbs::for('edit', function ($trail, $categories) {
    $trail->parent('categories');
    $trail->push($categories->name, route('categories.show', $categories->getKey()));
    $trail->push('Güncelle');
});

// Tags Breadcrumbs
Breadcrumbs::for('tags', function ($trail) {
    $trail->parent('home');
    $trail->push('Etiketler', route('tags.index'));
});
