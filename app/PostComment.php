<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostComment extends Model
{
    protected $table = 'post_comment';
    protected $fillable = [
        'post_id','content','status','user_id'
    ];
    protected $attributes = ['status'=>1];
    protected $dates = ['created_at','updated_at'];

    public function post()
    {
        return $this->belongsTo('App\Post');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
