<?php

    namespace App;

    use Illuminate\Database\Eloquent\Model;

    /**
     * Class Gallery
     * @package App
     * @mixin \Illuminate\Database\Eloquent\Builder
     */
    class Gallery extends Model
    {
        const UPDATED_AT = null;

        /**
         * @var string
         */
        public $table = 'gallery';

        /**
         * @var string[]
         */
        protected $fillable = [
            'post_id','name','file_name','user_id'
        ];

        /**
         * @var string[]
         */
        protected $dates = ['created_at','updated_at'];

        /**
         *
         * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function post()
        {
            return $this->belongsTo('App\Post');
        }

        /**
         *
         * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function user()
        {
            return $this->belongsTo('App\User');
        }
    }
