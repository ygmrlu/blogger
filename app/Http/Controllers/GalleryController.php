<?php

namespace App\Http\Controllers;

use App\Gallery;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response|View
     */
    public function index($id)
    {
        $post = Post::findOrFail($id);

        $gallery = Gallery::where('post_id', $post->getKey())->get();
        return view('gallery.index', compact('gallery', 'post'));
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     */
    public function show($id)
    {
        $gallery = Gallery::findOrFail($id);

        $folder = public_path('uploads');

        $path = $folder . DIRECTORY_SEPARATOR . $gallery->file_name;
        if (file_exists($path)){
            return response()->file($path);
        }
        else {
            return redirect()->route('gallery.index', $gallery->post_id)->withToastError('Dosya mevcut değil!');
        }
    }

    public function download($id){
        $gallery = Gallery::findOrFail($id);

        $folder = public_path('uploads');
        $path = $folder . DIRECTORY_SEPARATOR . $gallery->file_name;

        if (file_exists($path))
        {
            return response()->download($path);
        }else{
            return redirect()->route('gallery.index', $gallery->post_id)->withToastError('Dosya mevcut değil!');
        }
    }

    public function upload($id, Request $request)
    {
        $post = Post::findOrFail($id);

        if ($request->hasFile('file'))
        {
            // Upload path
            $destinationPath = 'uploads/';

            // Create directory if not exists
            if (!is_dir($destinationPath))
            {
                @mkdir($destinationPath, 0755, true);
            }

            // Get file extension
            $extension = $request->file('file')->getClientOriginalExtension();

            // Valid extensions
            $validExtensions = ['jpeg','jpg','png','pdf'];

            // Check extension
            if(in_array(strtolower($extension), $validExtensions))
            {
                // Rename file
                $fileName = uniqid().time().'.'.$extension;

                // Uploading file to given path
                $request->file('file')->move($destinationPath, $fileName);

                $gallery = new Gallery();
                $gallery->post_id = $post->getKey();
                $gallery->name = $request->file('file')->getClientOriginalName();
                $gallery->file_name = $fileName;
                $gallery->user_id = Auth::id();
                $gallery->save();
            }
        }
    }

    public function refresh($id)
    {
        $post = Post::findOrFail($id);

        $gallery = Gallery::where('post_id', $post->getKey())->get();
        return view('gallery.view.items', compact('gallery'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Gallery  $gallery
     */
    public function destroy(Gallery $gallery)
    {
        $post = Post::findOrFail($gallery->post_id);

        $folder = public_path('uploads');
        $path = $folder. DIRECTORY_SEPARATOR . $gallery->file_name;
        if (file_exists($path))
        {
            @unlink($path);
        }

        $gallery->delete();

        return redirect()->route('gallery.index', $post->getKey())->withToastSuccess('Silme İşlemi Gerçekleşti.');
    }
}
