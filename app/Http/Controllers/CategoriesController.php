<?php

namespace App\Http\Controllers;

use App\Categories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use mysql_xdevapi\Exception;

class CategoriesController extends Controller
{
    /**
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @date      1.08.2020
     */
    public function index()
    {
        $categories = Categories::latest()->paginate(5);
        return view('categories.index', compact('categories'))->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:categories|max:128',
        ]);

        if ($validator->fails())
        {
            $errorMessages = $validator->errors()->getMessages();

            if ($request->ajax())
            {
                return \response()->json(['errors'=>$errorMessages]);
            }else{
                return back()->withErrors($validator)->withInput();
            }
        }

        Categories::create($request->all());

        return redirect()->route('categories.index')->with('success', 'İşlem Başarıyla Gerçekleşti.');
    }

    public function show(Categories $categories)
    {
        return view('categories.show', compact('categories'));
    }

    public function edit(Categories $categories)
    {
        return view('categories.edit', compact('categories'));
    }

    public function update(Request $request, Categories $categories)
    {
        $exists = $categories->where('name', $request->input('name'))
            ->whereNotIn($categories->getKeyName(), [$categories->getKey()])
            ->exists();

        if ($exists)
        {
            $request->session()->flash('error', 'Bu kategori daha önce eklenmiş.');
            return redirect()->back();
        }

        $request->validate([
            'name' => 'required',
        ]);

        $categories->update($request->all());

        return redirect()->route('categories.show', $categories->getKey())->with('success', 'İşlem Başarıyla Gerçekleşti.');
    }

    public function destroy(Categories $categories)
    {
        DB::beginTransaction();

        try {
            $categories->delete();

            DB::commit();

            return redirect()->route('categories.index')->with('success', 'Silme İşlemi Gerçekleşti.');
        }catch (\Exception $exception){
            DB::rollBack();

            throw new Exception($exception->getMessage());
        }

    }
}
