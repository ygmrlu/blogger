<?php

namespace App\Http\Controllers;

use App\Tags;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class TagsController extends Controller
{
    public function index()
    {
        $tags = Tags::orderBy('created_at', 'DESC')->get();
        return view('tags.index', compact('tags'));
    }

    public function store(Request $request)
    {
        $exists = Tags::where('name', $request->input('name'))->exists();
        if ($exists)
        {
            $request->session()->flash('error', 'Bu etiket daha önce eklenmiş.');
            return redirect()->back();
        }

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:128',
        ]);

        if ($validator->fails())
        {
            $errorMessages = $validator->errors()->getMessages();

            if ($request->ajax())
            {
                return \response()->json(['errors'=>$errorMessages]);
            }else{
                return back()->withErrors($validator)->withInput();
            }

        }

        Tags::create($request->all());

        return redirect()->route('tags.index')->with('success', 'İşlem Başarıyla Gerçekleşti.');
    }

    public function destroy(Tags $tags)
    {
        DB::beginTransaction();

        try {
            $tags->delete();

            DB::commit();

            return redirect()->route('tags.index')->with('success', 'Silme İşlemi Gerçekleşti.');
        }
        catch (\Exception $e)
        {
            DB::rollBack();
            throw new \Exception($e->getMessage());
        }
    }
}
