<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    public function index()
    {
        $posts = Post::latest()->paginate(5);
        return view('post.index', compact('posts'))->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function create()
    {
        return view('post.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title'   => 'required|max:255',
            'content' => 'required'
        ]);

        if ($validator->fails())
        {
            $errorMessages = $validator->errors()->getMessages();

            if ($request->ajax())
            {
                return \response()->json(['errors'=>$errorMessages]);
            }else{
                return back()->withErrors($validator)->withInput();
            }

        }

        $post = new Post();
        $post->title = $request->input('title');
        $post->content = $request->input('content');
        $post->is_published = $request->input('is_published');
        $post->user_id = Auth::id();
        $post->save();

        return redirect()->route('post.index')->with('success', 'İşlem Başarıyla Gerçekleşti.');
    }

    public function show(Post $post)
    {
        return view('post.show', compact('post'));
    }

    public function edit(Post $post)
    {
        return view('post.edit', compact('post'));
    }

    public function update(Request $request, Post $post)
    {
        $request->validate([
            'title'   => 'required|max:255',
            'content' => 'required'
        ]);

        $post->update($request->all());

        //return redirect()->route('post.show', $post->getKey())->with('success', 'İşlem Başarıyla Gerçekleşti.');
        return redirect()->route('post.show', $post->getKey())->withToastSuccess('İşlem Başarıyla Gerçekleşti.');
    }

    public function destroy(Post $post)
    {
        // post siler
        // post yorumlarını siler
    }
}
