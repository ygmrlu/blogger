<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Hata - {{ $exception->getStatusCode() }}</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,700" rel="stylesheet">

    <!-- Stylesheet -->
    <link href="{{ asset('css/error.css') }}" rel="stylesheet">

</head>

@php
    $message = $exception->getMessage();
@endphp

<body>
    <div id="notfound">
        <div class="notfound">
            <div class="notfound-404">
                <h1>4<span>0</span>4</h1>
            </div>
            <p>{{ $message ? $message : 'Sayfa Bulunamadı' }}</p>
            <a href="{{ url('/') }}">Anasayfa</a>
        </div>
    </div>
</body>

</html>
