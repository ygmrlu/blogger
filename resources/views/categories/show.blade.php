@extends('layouts.app')

@section('title', $categories->name)

@section('breadcrumbs', \DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('show', $categories->name))

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="main-card mb-3 card">
                <div class="card-body table-responsive">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th style="width: 20%">Kategori Adı</th>
                            <td style="width: auto">{{ $categories->name }}</td>
                        </tr>
                        <tr>
                            <th>Seo Slug</th>
                            <td>{{ $categories->seo_slug }}</td>
                        </tr>
                        <tr>
                            <th>Durum</th>
                            <td>{{ $categories->status == 1 ? 'Aktif' : 'Pasif' }}</td>
                        </tr>
                        <tr>
                            <th>Eklenme Tarihi</th>
                            <td>{{ $categories->created_at->format('d.m.Y H:i:s') }}</td>
                        </tr>
                        <tr>
                            <th>Güncellenme Tarihi</th>
                            <td>{{ $categories->created_at->format('d.m.Y H:i:s') }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
