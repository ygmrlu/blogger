@extends('layouts.app')

@section('title', $categories->name)

@section('breadcrumbs', \DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('edit', $categories))

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <div class="mt-10">
                        <form action="{{ route('categories.update', $categories->getKey()) }}" method="POST">
                            @csrf
                            @method('PUT')

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <label for="name">Kategori Adı</label>
                                        <input type="text" class="form-control" id="name" name="name" value="{{ $categories->name }}">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <label for="status">Durum</label>
                                        <select class="form-control" id="status" name="status">
                                            <option value="1" {{ $categories->status == 1 ? "selected=selected" : null }}>Aktif</option>
                                            <option value="0" {{ $categories->status == 0 ? "selected=selected" : null }}>Pasif</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <button type="submit" class="btn btn-primary">Güncelle</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
