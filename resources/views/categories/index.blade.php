@extends('layouts.app')

@section('title', 'Kategoriler')

@section('breadcrumbs', \DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('categories'))

@section('content')
    <div class="row">
        <div class="col-md-7">
            <div class="mb-3 card">
                <div class="card-body">
                    <div class="table-responsive">
                        @if ($categories->count() > 0)
                            <p><b>Toplam Kayıt Sayısı:</b> {{ $categories->count() }}</p>
                        @endif

                        <table class="table table-bordered table-striped table-hover mb-0">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Adı</th>
                                <th>Eklenme Tarihi</th>
                                <th>Durum</th>
                                <th style="width: 20%"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse ($categories as $item)
                                <tr>
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->created_at->format('d.m.Y H:i:s') }}</td>
                                    <td>{{ $item->status == 0 ? 'Pasif' : 'Aktif' }}</td>
                                    <td style="text-align: center;">
                                        <form action="{{ route('categories.destroy', $item->getKey()) }}" method="POST">
                                            <a class="btn btn-info btn-sm" title="Görüntüle" href="{{ route('categories.show', $item->getKey()) }}">
                                                <i class="fa fa-eye"></i>
                                            </a>

                                            <a class="btn btn-primary btn-sm" title="Düzenle" href="{{ route('categories.edit', $item->getKey()) }}">
                                                <i class="fa fa-edit"></i>
                                            </a>

                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Bu kategoriyi silmek istediğinize emin misiniz?')">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="5">Kayıt Bulunamadı.</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>

                    <div class="mt-3">
                        {!! $categories->links() !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="mb-3 card">
                <div class="card-header">Yeni Kategori</div>
                <form method="POST" action="{{ route('categories.store') }}" role="form" autocomplete="off">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="name">Kategori Adı</label>
                            <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="name" maxlength="128" placeholder="Kategori Adı" value="{{ old('name') }}">

                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Kaydet</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
