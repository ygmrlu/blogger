@extends('layouts.app')

@section('title', 'Yeni Kategori')

@section('content')
    <div class="card-header">
        @yield('title')
    </div>
    <form method="POST" action="{{ route('categories.store') }}" role="form" autocomplete="off">
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label for="name">Kategori Adı</label>
                <input type="text" class="form-control" name="name" id="name" value="{{ old('name') }}" placeholder="Kategori Adı">
            </div>
        </div>

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Kaydet</button>
        </div>
    </form>
@endsection
