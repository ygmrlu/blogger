<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title"><strong>{{ $post->title  }}</strong> ait resimler</h3>
            </div>
            <div class="card-body">
                @if ($gallery->count() > 0)
                    <p><b>Toplam Resim Sayısı:</b> {{ $gallery->count() }}</p>
                @endif

                <table class="table table-bordered table-striped table-hover mb-0">
                    <thead>
                    <tr>
                        <th>Adı</th>
                        <th>Ekleyen Kullanıcı</th>
                        <th>Eklenme Tarihi</th>
                        <th>Güncellenme Tarihi</th>
                        <th style="width: 20%"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse ($gallery as $item)
                        <tr>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->user->name }}</td>
                            <td>{{ $item->created_at->format('d.m.Y H:i:s') }}</td>
                            <td>{{ isset($item->updated_at) && $item->updated_at != '' ? $item->updated_at->format('d.m.Y H:i:s') : '-' }}</td>
                            <td style="text-align: center;">
                                <form action="{{ route('gallery.destroy', $item->getKey()) }}" method="POST">
                                    <a class="btn btn-info btn-sm" title="Görüntüle" href="{{ route('gallery.show', $item->getKey()) }}" target="_blank">
                                        <i class="fa fa-eye"></i>
                                        Görüntüle
                                    </a>

                                    <a class="btn btn-primary btn-sm" title="Görüntüle" href="{{ route('gallery.download', $item->getKey()) }}">
                                        <i class="fa fa-download"></i>
                                        İndir
                                    </a>

                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Bu resmi silmek istediğinize emin misiniz?')">
                                        <i class="fa fa-trash"></i>
                                        Sil
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="5">Kayıt Bulunamadı.</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
