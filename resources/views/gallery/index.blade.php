@php
    /* @var $post \App\Post */
@endphp

@extends('layouts.app')

@section('title', $post->title)

@section('actions')
    <a href="{{ route('post.show', $post->getKey()) }}" class="btn btn-success"><i class="fa fa-arrow-left"></i> Post</a>
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('dropzone/dist/min/dropzone.min.css') }}">
@endpush

@push('scripts')
    <script src="{{ asset('dropzone/dist/min/dropzone.min.js') }}" type="text/javascript"></script>
    <script>
        var CSRF_TOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute("content");

        Dropzone.options.dropzone = {
            acceptedFiles: ".jpeg, .jpg, .png, .pdf",
            complete: function (file) {
                var $data_url = $("#dropzone").data("url");

                $.post($data_url, {_token: CSRF_TOKEN}, function(response){

                    $(".gallery-list-container").html(response);

                });
            }
        };
    </script>
@endpush

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="mb-3 card">
                <div class="card-body">
                    <form action="{{ route('gallery.upload', $post->getKey()) }}" id="dropzone" class="dropzone" data-plugin="dropzone" data-url="{{ route('gallery.refresh', $post->getKey()) }}">
                        @csrf
                        <div class="dz-message">
                            <h3 class="m-h-lg">Yüklemek istediğiniz resimleri buraya sürükleyiniz</h3>
                            <p class="m-b-lg text-muted">(Yüklemek için dosyalarınızı sürükleyiniz yada buraya tıklayınız)</p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="gallery-list-container">
        @include('gallery.view.items', ['gallery' => $gallery, 'post' => $post])
    </div>
@endsection
