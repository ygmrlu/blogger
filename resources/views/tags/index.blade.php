@extends('layouts.app')

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/tags.css') }}">
@endpush

@section('title', 'Etiketler')

@section('breadcrumbs', \DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('tags'))

@section('content')
    <div class="row">
        <div class="col-md-7">
            <div class="mb-3 card">
                <div class="card-body">
                    <div class="table-responsive">
                        @if ($tags->count() > 0)
                            <p><b>Toplam Kayıt Sayısı:</b> {{ $tags->count() }}</p>
                        @endif

                        <table class="table table-bordered table-striped table-hover mb-0">
                            <thead>
                            <tr>
                                <th>Etiket</th>
                                <th>Oluşturulma Tarihi</th>
                                <th style="width: 20%"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse ($tags as $item)
                                <tr>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->created_at->format('d.m.Y H:i:s') }}</td>
                                    <td style="text-align: center;">
                                        <form action="{{ route('tags.destroy', $item->getKey()) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Bu etiketi silmek istediğinize emin misiniz?')">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="3">Kayıt Bulunamadı.</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="mb-3 card">
                <div class="card-header">Yeni Etiket Oluştur</div>
                <form method="POST" action="{{ route('tags.store') }}" role="form" autocomplete="off">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="name">Etiket Adı</label>
                            <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="name" maxlength="128" placeholder="Etiket" value="{{ old('name') }}">

                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Oluştur</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
