@extends('layouts.app')

@section('title', $post->title)

@section('actions')
    <a href="{{ route('gallery.index', $post->getKey()) }}" class="btn btn-primary"><i class="fa fa-arrow-right"></i> Galeriye Git</a>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="mb-3 card">
                <div class="card-header">
                    <span class="text-bold">İçerik</span>
                </div>
                <div class="card-body">
                    {!! $post->content !!}
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="mb-3 card">
                <div class="card-header">
                    <span class="text-bold">Diğer Bilgiler</span>
                </div>
                <div class="card-body table-responsive">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th style="width: 20%">Başlık</th>
                            <td style="width: auto">{{ $post->title }}</td>
                        </tr>
                        <tr>
                            <th>Seo Slug</th>
                            <td>{{ $post->seo_slug }}</td>
                        </tr>
                        <tr>
                            <th>Yayında</th>
                            <td>{{ $post->is_published == 1 ? 'Evet' : 'Hayır' }}</td>
                        </tr>
                        <tr>
                            <th>Oluşturan Kullanıcı</th>
                            <td>{{ $post->user->name }}</td>
                        </tr>
                        <tr>
                            <th>Eklenme Tarihi</th>
                            <td>{{ $post->created_at->format('d.m.Y H:i:s') }}</td>
                        </tr>
                        <tr>
                            <th>Güncellenme Tarihi</th>
                            <td>{{ $post->created_at->format('d.m.Y H:i:s') }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
