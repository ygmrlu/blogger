@extends('layouts.app')

@section('title', 'Postlar')

@section('actions')
    <a href="{{ route('post.create') }}" class="btn btn-success"><i class="fa fa-plus"></i> Yeni Oluştur</a>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="mb-3 card">
                <div class="card-body">
                    <div class="table-responsive">
                        @if ($posts->count() > 0)
                            <p><b>Toplam Kayıt Sayısı:</b> {{ $posts->count() }}</p>
                        @endif

                        <table class="table table-bordered table-striped table-hover mb-0">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Başlık</th>
                                <th>Yayında</th>
                                <th>Oluşturan Kullanıcı</th>
                                <th>Eklenme Tarihi</th>
                                <th style="width: 20%"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse ($posts as $item)
                                <tr>
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $item->title }}</td>
                                    <td>{{ $item->is_published == 1 ? 'Evet' : 'Hayır' }}</td>
                                    <td>{{ $item->user->name }}</td>
                                    <td>{{ $item->created_at->format('d.m.Y H:i:s') }}</td>
                                    <td style="text-align: center;">
                                        <form action="{{ route('post.destroy', $item->getKey()) }}" method="POST">
                                            <a class="btn btn-info btn-sm" title="Görüntüle" href="{{ route('post.show', $item->getKey()) }}">
                                                <i class="fa fa-eye"></i>
                                            </a>

                                            <a class="btn btn-primary btn-sm" title="Düzenle" href="{{ route('post.edit', $item->getKey()) }}">
                                                <i class="fa fa-edit"></i>
                                            </a>

                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Bu postu silmek istediğinize emin misiniz?')">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="6">Kayıt Bulunamadı.</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>

                    <div class="mt-3">
                        {!! $posts->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
