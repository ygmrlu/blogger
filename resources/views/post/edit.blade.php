@extends('layouts.app')

@section('title', $post->title)

@push('scripts')
    <script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
    <script>
        setTimeout(function () {
            CKEDITOR.replace('content');
        }, 400);
    </script>
@endpush

@section('content')
    <div class="card">
        <div class="card-header">
            @yield('title')
        </div>
        <div class="card-body">
            <form method="POST" action="{{ route('post.update', $post->getKey()) }}">
                @csrf
                @method('PUT')
                <div class="card-body">
                    <div class="form-group">
                        <label for="title">Başlık</label>
                        <input type="text" class="form-control" name="title" id="title" maxlength="255" placeholder="Başlık" value="{{ $post->title ? $post->title : old('title') }}">
                    </div>

                    <div class="form-group">
                        <label for="content">İçerik</label>
                        <textarea name="content" class="form-control" id="content" cols="30" rows="10">{{ $post->content }}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="is_published">Yayında</label>
                        <select class="form-control" name="is_published" id="is_published">
                            <option value="1" {{ $post->is_published == 1 ? "selected=selected" : null }}>Evet</option>
                            <option value="0" {{ $post->is_published == 0 ? "selected=selected" : null }}>Hayır</option>
                        </select>
                    </div>
                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Kaydet</button>
                </div>
            </form>
        </div>
    </div>
@endsection
